package com.crud.practice.domain.dto;

import com.crud.practice.domain.dto.ClientDto;

import java.io.Serializable;

public class ProfileDto implements Serializable {
    ClientDto clientDto;
    String imagen = "Not Asigned";

    //---------------- Getters and setters -----------------

    public ClientDto getClientDto() {
        return clientDto;
    }

    public void setClientDto(ClientDto clientDto) {
        this.clientDto = clientDto;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
