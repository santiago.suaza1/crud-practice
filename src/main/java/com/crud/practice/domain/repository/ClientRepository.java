package com.crud.practice.domain.repository;

import com.crud.practice.domain.dto.ClientDto;

import java.util.List;
import java.util.Optional;

public interface ClientRepository {
    List<ClientDto> getAll();
    List<ClientDto> getByAge(int age);
    Optional<ClientDto> getClient(int ClientId);
    ClientDto save(ClientDto client);
    void delete(int ClientId);
}
