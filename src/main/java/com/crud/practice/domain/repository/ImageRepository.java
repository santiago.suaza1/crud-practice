package com.crud.practice.domain.repository;

import com.crud.practice.domain.dto.ImageDto;

import java.util.List;
import java.util.Optional;

public interface ImageRepository {
    List<ImageDto> getAll();
    Optional<ImageDto> getImage(String imageId);
    ImageDto save(ImageDto image);
    List<ImageDto> getByClientId(String idCliente);
    void delete(String imageId);
}
