package com.crud.practice.domain.service;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.domain.dto.ProfileDto;
import com.crud.practice.domain.repository.ClientRepository;
import com.crud.practice.manageExceptions.customExceptions.NotSuchElementFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ImageService imageService;

    public List<ClientDto> getAll(int age) {
        if (age!=0) return clientRepository.getByAge(age);
        else return clientRepository.getAll();
    }

    public ClientDto getClient(int clientId){
        Optional<ClientDto> optClientDto = clientRepository.getClient(clientId);
        if (!optClientDto.isPresent()) throw new NotSuchElementFoundException("Not client found");
        return optClientDto.get();
    }

    public List<ClientDto> findByAge(int age){
      return clientRepository.getByAge(age);
    }

    public ProfileDto save(ClientDto clientDto, MultipartFile photo) {
        ClientDto savedClient = clientRepository.save(clientDto);
        ProfileDto profileDto = new ProfileDto();
        profileDto.setClientDto(savedClient);
        if (!(photo == null)) {
            profileDto.setImagen(imageService.save(
                    photo,
                    clientDto.getName(),
                    savedClient.getClientId()).getPhoto());
        }
        return profileDto;
    }

    public ClientDto update(int clientId, ClientDto newInfoClient){
        ClientDto oldClientDto = getClient(clientId);
        ClientDto newClientDto = oldClientDto;
        newClientDto.setName(newInfoClient.getName());
        newClientDto.setLastName(newInfoClient.getLastName());
        newClientDto.setAge(newInfoClient.getAge());
        newClientDto.setCityOfBirth(newInfoClient.getCityOfBirth());
        return clientRepository.save(newClientDto);
    }

    public void delete(int clientId){
        getClient(clientId);
        imageService.deleteImagesFromClient(clientId);
        clientRepository.delete(clientId);
    }
}
