package com.crud.practice.domain.service;

import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.domain.repository.ImageRepository;
import com.crud.practice.manageExceptions.customExceptions.NotAvailableImageFormatException;
import com.crud.practice.manageExceptions.customExceptions.NotSuchElementFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;

    public List<ImageDto> getAll(){
        return imageRepository.getAll();
    }

    public ImageDto getImage(String imageId){
        Optional<ImageDto> optImageDto = imageRepository.getImage(imageId);
        if(optImageDto.isPresent()) return optImageDto.get();
        else throw new NotSuchElementFoundException("Not image with that id found");
    }

    public ImageDto save(ImageDto image){
        return imageRepository.save(image);
    }

    public ImageDto save(MultipartFile photo, String userName, int clientId){
        String photoOnBytesString = "";
        try {
            photoOnBytesString = photo.getBytes().toString();
        } catch (IOException ex) {
            throw new NotAvailableImageFormatException("Client image format not accepted");
        }
        ImageDto imageDto = new ImageDto();
        imageDto.setPhoto(photoOnBytesString);
        imageDto.setClientId(String.valueOf(clientId));
        imageDto.setName(userName+ UUID.randomUUID());

        return imageRepository.save(imageDto);
    }

    public ImageDto update(String imageId, ImageDto newInfoImage){
        ImageDto oldImageDto = getImage(imageId);
        ImageDto newImageDto = oldImageDto;
        newImageDto.setType(newInfoImage.getType());
        newImageDto.setName(newInfoImage.getName());
        newImageDto.setPhoto(newInfoImage.getPhoto());
        newImageDto.setClientId(newInfoImage.getClientId());
        return imageRepository.save(newImageDto);
    }

    public List<ImageDto> getByClientId(int clientId){
        return imageRepository.getByClientId(String.valueOf(clientId));
    }

    public void delete(String imageId){
        getImage(imageId);
        imageRepository.delete(imageId);
    }

    public void deleteImagesFromClient(int clientId){
        for (ImageDto image:getByClientId(clientId)) delete(image.getImageId());
    }
}
