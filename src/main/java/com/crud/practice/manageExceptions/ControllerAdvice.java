package com.crud.practice.manageExceptions;

import com.crud.practice.manageExceptions.customExceptions.NotAvailableImageFormatException;
import com.crud.practice.manageExceptions.customExceptions.NotSuchElementFoundException;
import com.crud.practice.utils.SqlExceptionMessagesConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler({
            NotSuchElementFoundException.class, NotAvailableImageFormatException.class
    })
    public ResponseEntity<ErrorDto> NotSuchElementFoundHandler(ResponseStatusException exception){
        return new ResponseEntity<ErrorDto>(
                ErrorDto.builder().messageError(exception.getReason()).build(),
                exception.getStatus()
        );
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public ResponseEntity SQLIntegrityConstraintViolationHandler(SQLIntegrityConstraintViolationException exception){
        HashMap<String,String> reason = SqlExceptionMessagesConverter.parseConstraintViolated(exception.getMessage());
        return new ResponseEntity<>(
                "The "+attributeToString(reason.get("attribute"))+" must be unique",
                HttpStatus.BAD_REQUEST
        );
    }

    private String attributeToString(String attributeName){
        switch (attributeName){
            case "numero_identificacion": return "identification number";
        }
        return "";
    }

}
