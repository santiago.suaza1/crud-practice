package com.crud.practice.manageExceptions;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDto{
    private String messageError;
}
