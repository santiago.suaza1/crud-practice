package com.crud.practice.manageExceptions.customExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotSuchElementFoundException extends ResponseStatusException {

    public NotSuchElementFoundException(String reason) {
        super(HttpStatus.NOT_FOUND, reason);
    }
}
