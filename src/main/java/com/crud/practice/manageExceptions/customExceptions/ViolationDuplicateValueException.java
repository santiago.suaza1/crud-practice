package com.crud.practice.manageExceptions.customExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ViolationDuplicateValueException extends ResponseStatusException {

    public ViolationDuplicateValueException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
