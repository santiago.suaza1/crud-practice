package com.crud.practice.persistence;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.domain.repository.ClientRepository;
import com.crud.practice.manageExceptions.customExceptions.ViolationDuplicateValueException;
import com.crud.practice.persistence.crud.ClienteCrudRepository;
import com.crud.practice.persistence.crud.ImagenCrudRepository;
import com.crud.practice.persistence.entity.Cliente;
import com.crud.practice.persistence.mapper.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;

@Repository
public class ClienteRepository implements ClientRepository {

    @Autowired
    private ClienteCrudRepository clienteCrudRepository;

    @Autowired
    private ImagenCrudRepository imagenCrudRepository;

    @Autowired
    private ClientMapper mapper;

    @Override
    public List<ClientDto> getAll() {
        List<Cliente> clientes = (List<Cliente>) clienteCrudRepository.findAll();
        return mapper.toClients(clientes);
    }

    @Override
    public List<ClientDto> getByAge(int edad) {
        return mapper.toClients(clienteCrudRepository.findByEdad(edad));
    }

    @Override
    public Optional<ClientDto> getClient(int clientId) {
        return clienteCrudRepository.findById(clientId).map(cliente -> mapper.toClient(cliente));
    }

    @Override
    public ClientDto save(ClientDto client) {
        Cliente cliente = mapper.toCliente(client);
        Cliente clienteGuardado;
        clienteGuardado = clienteCrudRepository.save(cliente);

        return mapper.toClient(clienteGuardado);
    }

    @Override
    public void delete(int clientId) {
        clienteCrudRepository.deleteById(clientId);
    }

}
