package com.crud.practice.persistence;

import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.domain.repository.ImageRepository;
import com.crud.practice.persistence.crud.ImagenCrudRepository;
import com.crud.practice.persistence.entity.Imagen;
import com.crud.practice.persistence.mapper.ImageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class ImagenRepository implements ImageRepository {

    @Autowired
    private ImagenCrudRepository imagenCrudRepository;

    @Autowired
    private ImageMapper mapper;

    @Override
    public List<ImageDto> getAll(){
        return mapper.toImages((List<Imagen>) imagenCrudRepository.findAll());
    }

    @Override
    public Optional<ImageDto> getImage(String imageId) {
        return imagenCrudRepository.findById(imageId).map(imagen -> mapper.toImage(imagen));
    }

    @Override
    public ImageDto save(ImageDto image) {
        return mapper.toImage(imagenCrudRepository.save(mapper.toImagen(image)));
    }

    @Override
    public List<ImageDto> getByClientId(String clientId) {
        return mapper.toImages(imagenCrudRepository.findByIdCliente(clientId));
    }

    @Override
    public void delete(String imageId) {
        imagenCrudRepository.deleteById(imageId);
    }
}
