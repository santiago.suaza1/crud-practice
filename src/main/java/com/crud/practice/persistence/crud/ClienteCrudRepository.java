package com.crud.practice.persistence.crud;

import com.crud.practice.persistence.entity.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClienteCrudRepository extends CrudRepository<Cliente, Integer> {

    List<Cliente> findByEdad(int edad);
    Cliente findByNumeroIdentificacion(String numIdentificacion);
    //List<Cliente> findAllOrderByEdadAsc();
}
