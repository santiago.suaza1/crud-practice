package com.crud.practice.persistence.crud;

import com.crud.practice.persistence.entity.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ImagenCrudRepository extends MongoRepository<Imagen, String> {
    List<Imagen> findByIdCliente(String idCliente);
}
