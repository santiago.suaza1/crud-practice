package com.crud.practice.persistence.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;


@Document("imagenes")
public class Imagen {

    @Id
    private String id;

    private String tipo;

    private String nombre;

    private String foto;

    private String idCliente;


    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
