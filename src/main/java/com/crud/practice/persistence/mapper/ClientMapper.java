package com.crud.practice.persistence.mapper;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.persistence.entity.Cliente;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

//Si hay que usar una clase interna que tengo otro mappeador hay que indicarselo con
//"uses = {ImageMapper.class}"
@Mapper(componentModel = "Spring", uses = {ImageMapper.class})
public interface ClientMapper {
    @Mappings({
            @Mapping(source = "cliente.id", target = "clientId"),
            @Mapping(source = "cliente.nombre", target = "name"),
            @Mapping(source = "cliente.apellidos", target = "lastName"),
            @Mapping(source = "cliente.tipoIdentificacion", target = "identificationType"),
            @Mapping(source = "cliente.numeroIdentificacion", target = "identificationNumber"),
            @Mapping(source = "cliente.edad", target = "age"),
            @Mapping(source = "cliente.ciudadNacimiento", target = "cityOfBirth"),
            //@Mapping(source = "imagen", target = "image")
    })
    ClientDto toClient(Cliente cliente);
    List<ClientDto> toClients(List<Cliente> clientes);

    @InheritInverseConfiguration
    Cliente toCliente(ClientDto client);
}
