package com.crud.practice.persistence.mapper;

import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.persistence.entity.Imagen;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    @Mappings({
            @Mapping(source = "id", target = "imageId"),
            @Mapping(source = "tipo", target = "type"),
            @Mapping(source = "nombre", target = "name"),
            @Mapping(source = "foto", target = "photo"),
            @Mapping(source = "idCliente", target = "clientId")
    })
    ImageDto toImage(Imagen imagen);
    List<ImageDto> toImages(List<Imagen> imagenes);

    @InheritInverseConfiguration
    Imagen toImagen(ImageDto image);
}
