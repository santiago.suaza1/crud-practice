package com.crud.practice.utils;

import java.util.HashMap;

public class SqlExceptionMessagesConverter {
    @SuppressWarnings("unchecked")
    public static HashMap<String, String> parseConstraintViolated(String messageError){
        String constraintViolated = messageError.split("key")[1];
        String withoutComiles = constraintViolated.replaceAll("'", "");
        String [] splited = withoutComiles.split("\\.");
        HashMap<String, String> retorno = new HashMap();
        retorno.put("entity", splited[0]);
        retorno.put("attribute", splited[1]);
        return retorno;
    }
}
