package com.crud.practice.web.controller;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.domain.dto.ProfileDto;
import com.crud.practice.domain.service.ClientService;
import com.crud.practice.domain.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;


@RestController
@RequestMapping("/clients")
@Validated
public class ClientController {

    @Autowired
    private ClientService clientService;

    @Autowired
    private ImageService imageService;

    @GetMapping()
    public ResponseEntity<List<ClientDto>> getAll(@RequestParam(name = "age", required = false, defaultValue = "0") int age){
        return new ResponseEntity<>(clientService.getAll(age), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDto> getClient(@PathVariable("id") int clientId){
        return new ResponseEntity<>(clientService.getClient(clientId), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<ProfileDto> save(@RequestPart("client") ClientDto client, @RequestParam(value = "file", required = false)
            MultipartFile photo) {
        return new ResponseEntity<>(clientService.save(client, photo), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClientDto> update(@PathVariable("id") int clientId, @RequestBody ClientDto newInfoClient){
        return new ResponseEntity<>(clientService.update(clientId, newInfoClient), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") int clientId){
        clientService.delete(clientId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
