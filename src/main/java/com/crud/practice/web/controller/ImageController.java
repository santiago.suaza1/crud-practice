package com.crud.practice.web.controller;

import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.domain.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping()
    public ResponseEntity<List<ImageDto>> getAll(){
        return new ResponseEntity<>(imageService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ImageDto> getClient(@PathVariable("id") String imageId){
        return new ResponseEntity<>(imageService.getImage(imageId), HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<ImageDto> save(@RequestBody ImageDto client){
        return new ResponseEntity<>(imageService.save(client), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ImageDto> update(@PathVariable("id") String imageId , @RequestBody ImageDto newImageInfo){
        return new ResponseEntity<>(imageService.update(imageId, newImageInfo), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") String imageId){
        imageService.delete(imageId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
