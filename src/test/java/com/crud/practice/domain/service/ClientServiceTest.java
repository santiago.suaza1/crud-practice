package com.crud.practice.domain.service;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.domain.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    ClientDto clientDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        clientDto = new ClientDto();
        clientDto.setClientId(1);
        clientDto.setAge(22);
        clientDto.setName("Antonio");
        clientDto.setCityOfBirth("Amsterdanm");
        clientDto.setLastName("Rodriguez");
    }

    @Test
    void getAll() {
        when(clientRepository.getAll()).thenReturn(Arrays.asList(clientDto));

        assertNotNull(clientService.getAll());
        assertEquals(Arrays.asList(clientDto), clientService.getAll());
    }

    @Test
    void getClient() {
        when(clientRepository.getClient(1)).thenReturn(Optional.of(clientDto));

        assertNotNull(clientService.getClient(1));
        assertEquals(Optional.of(clientDto), clientService.getClient(1));
    }

    @Test
    void findByAge() {
        // Pendiente
    }

    @Test
    void save() {
        when(clientRepository.save(clientDto)).thenReturn(clientDto);
        /*lientDto serviceAnswer = clientService.save(clientDto);

        assertNotNull(serviceAnswer);
        assertEquals(clientDto, serviceAnswer);*/
    }

    @Test
    void update() {
        // Pendiente por cambios en la implementacion para limpiar la vista.
    }

    @Test
    void delete() {
        when(clientRepository.getClient(1)).thenReturn(Optional.of(clientDto));

        //assertEquals(true, clientService.delete(1));
        //assertEquals(false, clientService.delete(2));
    }
}