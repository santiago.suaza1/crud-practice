package com.crud.practice.web.controller;

import com.crud.practice.domain.dto.ClientDto;
import com.crud.practice.domain.dto.ProfileDto;
import com.crud.practice.domain.service.ClientService;
import com.crud.practice.domain.service.ImageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ClientControllerTest {

    @Mock
    private ClientService clientServiceMock;// = Mockito.mock(ClientService.class);

    @Mock
    private ImageService imageServiceMock;

    @InjectMocks
    private ClientController clientController;

    ClientDto clienteDto;
    ProfileDto profileDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        clienteDto = new ClientDto();
        clienteDto.setClientId(1);
        clienteDto.setAge(22);
        clienteDto.setName("Antonio");
        clienteDto.setCityOfBirth("Amsterdanm");
        clienteDto.setLastName("Rodriguez");

        profileDto = new ProfileDto();
        profileDto.setClientDto(clienteDto);
    }

    @Test
    void getAll() {
        Mockito.when(clientServiceMock.getAll()).thenReturn(Arrays.asList(clienteDto));
        ResponseEntity<List<ClientDto>> response = clientController.getAll();
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(Arrays.asList(clienteDto),response.getBody());
    }

    @Test
    void getClient() {
        Mockito.when(clientServiceMock.getClient(1)).thenReturn(clienteDto);
        ResponseEntity<ClientDto> response = clientController.getClient(1);
        ResponseEntity<ClientDto> responseFailed = clientController.getClient(2);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(clienteDto, response.getBody());

        assertEquals(null, responseFailed.getBody());
    }

    @Test
    void save() {
        MultipartFile file = new MockMultipartFile("name", (byte[]) null);
        Mockito.when(clientServiceMock.save(clienteDto, file)).thenReturn(profileDto);

        ResponseEntity<ProfileDto> response = clientController.save(clienteDto, file);

        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(profileDto, response.getBody());
        assertEquals("Not Asigned", response.getBody().getImagen());
    }

    @Test
    void update() {
        ClientDto clientDtoUpdated = clienteDto;
        clientDtoUpdated.setName("otro nombre");
        Mockito.when(clientServiceMock.update(1, clientDtoUpdated))
                .thenReturn(clientDtoUpdated);
        Mockito.when(clientServiceMock.getClient(1))
                .thenReturn(clienteDto);

        ResponseEntity<ClientDto> response = clientController.update(1, clientDtoUpdated);
        ResponseEntity responseFailed = clientController.delete(2);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(clientDtoUpdated, response.getBody());
    }

    @Test
    void delete() {
        ResponseEntity responseSuccess = clientController.delete(1);

        assertEquals(HttpStatus.NO_CONTENT, responseSuccess.getStatusCode());
    }
}