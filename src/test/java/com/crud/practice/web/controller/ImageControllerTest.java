package com.crud.practice.web.controller;

import com.crud.practice.domain.dto.ImageDto;
import com.crud.practice.domain.service.ImageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ImageControllerTest {

    @Mock
    ImageService imageServiceMock;

    @InjectMocks
    ImageController imageController;

    ImageDto imageDto;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imageDto = new ImageDto();
        imageDto.setImageId("620ac3de0563f805716834db");
        imageDto.setName("clientImage");
        imageDto.setPhoto("[B@56e04a86");
        imageDto.setType("png");
    }

    @Test
    void getAll() {
        Mockito.when(imageServiceMock.getAll()).thenReturn(Arrays.asList(imageDto));
        ResponseEntity<List<ImageDto>> response = imageController.getAll();

        assertEquals(Arrays.asList(imageDto), response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void getClient() {
        Mockito.when(imageServiceMock.getImage("620ac3de0563f805716834db"))
                .thenReturn(imageDto);
        ResponseEntity<ImageDto> responsesuccess = imageController.getClient("620ac3de0563f805716834db");
        ResponseEntity<ImageDto> responseFailed = imageController.getClient("Unexistence ID");

        assertEquals(HttpStatus.OK, responsesuccess.getStatusCode());
        assertEquals(imageDto, responsesuccess.getBody());

        //assertEquals(HttpStatus.NOT_FOUND, responseFailed.getStatusCode());

    }

    @Test
    void save() {
        Mockito.when(imageServiceMock.save(imageDto)).thenReturn(imageDto);
        ResponseEntity<ImageDto> responseSuccess = imageController.save(imageDto);

        assertEquals(HttpStatus.CREATED, responseSuccess.getStatusCode());
        assertEquals(imageDto, responseSuccess.getBody());
    }

    @Test
    void update() {
        ImageDto imageDtoUpdated = imageDto;
        imageDtoUpdated.setName("newName"+ UUID.randomUUID());
        Mockito.when(imageServiceMock.getImage("620ac3de0563f805716834db"))
                .thenReturn(imageDto);
        Mockito.when(imageServiceMock.update("620ac3de0563f805716834db", imageDtoUpdated))
                .thenReturn(imageDtoUpdated);

        ResponseEntity<ImageDto> responseSuccess = imageController.update(
                "620ac3de0563f805716834db",
                imageDtoUpdated
        );
        ResponseEntity<ImageDto> responseField = imageController.update(
                "non_existent",
                imageDtoUpdated
        );

        assertEquals(HttpStatus.OK, responseSuccess.getStatusCode());
        assertEquals(imageDtoUpdated, responseSuccess.getBody());

        //assertEquals(HttpStatus.NOT_FOUND, responseField.getStatusCode());
    }

    @Test
    void delete() {
        ResponseEntity responseSuccess = imageController.delete("620ac3de0563f805716834db");
        ResponseEntity responseFailed = imageController.delete("non-existent");

        assertEquals(HttpStatus.NO_CONTENT, responseSuccess.getStatusCode());
    }
}